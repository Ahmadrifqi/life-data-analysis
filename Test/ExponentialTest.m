function [B] = ExponentialTest(Data, r)
%% Bartlett's test for the Data Distribution
%% The Data will be tested with 
%% Exponential Distribution
%% Source:
%% An Introduction to Reliability and Maintainability Engineering
%% Charles E. Ebeling
%% 1997
%%
%% This Script was created by
%% Ahmad Rifqi

%% if nargin < 2 && nargin > 0
%%	r = length(Data);
%% end
%% den = 1 + (r+1)/(6*r);
%% num = 2*r*(log((1/r)*sum(Data)) - (sum(log(Data))/r));
%% B = num/den;

if nargin < 2 && nargin > 0
	r = length(Data);
end

% denominator calculation
den = 1 + (r+1)/(6*r);

% numerator calculation
num = 2*r*(log((1/r)*sum(Data)) - (sum(log(Data))/r));

B = num/den;

%% Value B then compared with the value of Chi square distribution
%% Chi square of (1-alpha/2, r-1) < B < Chi square of (alpha/2, r-1)
%% The chi square table can be accessed at:
%% https://people.richland.edu/james/lecture/m170/tbl-chi.html