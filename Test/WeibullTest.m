function[M, dof1, dof2] = WeibullTest(Raw, n);
%% Mann Testing for the Data Distribution
%% The Data will be tested with 
%% Two Parameters Weibull Distribution
%% Source:
%% An Introduction to Reliability and Maintainability Engineering
%% Charles E. Ebeling
%% 1997
%%
%% This Script was created by
%% Ahmad Rifqi
%%
%% Data = Raw;
%% Sorted_Data = sort(Data);
%% r = length(Data);
%% Z = zeros(length(Data), 1);
%% M = zeros(length(Data)-1, 1);
%% % Calculation of Z %
%% for i = 1:r
%%     Z(i) = log(-log(1-((i-0.5)/(n+2.5))));
%% end
%% M = diff(Z);
%% % Calculation of k %
%% k1 = floor(r/2);
%% k2 = floor((r-1)/2);
%% % Calculation of numerator and denominator for M
%% num = 0;
%% den = 0;
%% for i =k1+1:r-1
%% 	num = num + (log(Sorted_Data(i+1)) - log(Sorted_Data(i)))/M(i);
%% end
%% num = k1*num;
%% for i=1:k1
%%   den = den + (log(Sorted_Data(i+1)) - log(Sorted_Data(i)))/M(i);
%% end
%% den = k2*den;
%% M = num/den;
%% dof1 = 2*k1;
%% dof2 = 2*k2;


Data = Raw;
Sorted_Data = sort(Data);
r = length(Data);
Z = zeros(length(Data), 1);
M = zeros(length(Data)-1, 1);

% Calculation of Z %
for i = 1:r
    Z(i) = log(-log(1-((i-0.5)/(n+2.5))));
end
M = diff(Z);

% Calculation of k %
k1 = floor(r/2);
k2 = floor((r-1)/2);

% Calculation of numerator and denominator for M
num = 0;
den = 0;
for i =k1+1:r-1
	num = num + (log(Sorted_Data(i+1)) - log(Sorted_Data(i)))/M(i);
end
num = k1*num;

for i=1:k1
  den = den + (log(Sorted_Data(i+1)) - log(Sorted_Data(i)))/M(i);
endfor
den = k2*den;

M = num/den;
dof1 = 2*k1;
dof2 = 2*k2;

%% The M value must be lower than F crit
%% F crit is a function of alpha, dof1, and dof2
%% F crit table can be found in the book
%% or by using excel function of F.INV.RT(alpha; dof1; dof2)
%% or via website
%% http://www.socr.ucla.edu/Applets.dir/F_Table.html