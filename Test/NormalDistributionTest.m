%% Author: Nir Krakauer <mail@nirkrakauer.net>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Saphiro-Wilk Test for Normal Distribution                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [W, pval] = NormalDistributionTest(Data);
W = 0;
n = length(Data);
nh = floor (n/2);

% Sorting the data %
Sorted_Data = sort(Data);

% Mean Calculation %
Data_Avg = mean(Data);

% S squared calculation %
S_Squared = 0;
for i = 1:length(Data)
  S_Squared = S_Squared + (Sorted_Data(i) - Data_Avg)^2; 
end

% a coefficient calculation %
if n == 3
  a = sqrt(0.5); #exact
else
  #get an initial estimate of a
  m = -norminv(((1:nh)' - 0.375) / (n + 0.25));
  msq = 2*sumsq(m);
  mrms = sqrt (msq);
  #correction factors for initial elements of a (derived to be good approximations for 4 <= n <= 1000)
  rsn = 1 / sqrt(n);
  ac1 = m(1)/mrms + polyval ([-2.706056 4.434685 -2.07119 -0.147981 0.221157 0], rsn);
  if n <= 5
    phi = (msq - 2*m(1)^2) / (1 - 2*ac1^2);
    a = [ac1; m(2:nh)/sqrt(phi)];
  else
    ac2 = m(2)/mrms + polyval ([-3.582633 5.682633 -1.752461 -0.293762 0.042981 0], rsn);
    phi = (msq - 2*m(1)^2 - 2*m(2)^2) / (1 - 2*ac1^2 - 2*ac2^2);
    a = [ac1; ac2; m(3:nh)/sqrt(phi)];
  endif
endif

W = sum(a .* (Sorted_Data(n:-1:n+1-nh) - Sorted_Data(1:nh))).^2 / ...
    S_Squared;

#p value for the W statistic being as small as it is for a normal distribution
if n == 3
  pval = (6/pi) * (asin(sqrt(W)) - asin(sqrt(0.75)));
elseif n <= 11
  gamma = 0.459*n - 2.273;
  w = -log(gamma - log(1 - W));
  mu = polyval ([-0.0006714 0.025054 -0.39978 0.5440], gamma);
  sigma = exp (polyval([-0.0020322 0.062767 -0.77857 1.3822], n));
  pval = normcdf ((mu - w) / sigma);
else
  nl = log (n);
  w = log (1 - W);
  mu = polyval ([0.0038915 -0.083751 -0.31082 -1.5861], nl);
  sigma = exp (polyval ([0.0030302 -0.082676 -0.4803], nl));
  pval = normcdf ((mu - w) / sigma);
endif
endfunction