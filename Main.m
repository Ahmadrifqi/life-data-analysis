%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         This is the main script                              %
%   This script will be used to find tghe appropriate failure statistical pdf  %
%						   For a single component							   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This script is created by : Ahmad Rifqi									   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pkg load io; 
pkg load statistics; 
pkg load symbolic;
syms t
addpath("Utils");
addpath("Fitting");
addpath("Test");
%% File input and sheet pin-pointing
clc; clear all; close all;
filename = "Gangguan.xlsx";
sheet = "Wheelset";
disp("The minimum value of p value for alpha = 0.05 is 0.05");
disp("If the tested distribution does not meet the above value, thus there is not enough evidence"); 
disp("to conclude that the data have the tested distribution");
disp("Alpha means that the data have alpha percent correlation with the distribution");
%% Converting the excel file to txt file and taking account of the number of 
%% failures which hanppened to all the same component
name_file = txt_converter(filename, sheet);

%% loading the data
Data = load(name_file);
sorted_Data = sort(Data);

%% Testing the Data with normal distribution
[W, pvalNormal] = NormalDistributionTest(sorted_Data);
disp("p value for normal distribution test :");
disp(pvalNormal);

%% Testing the Data with log normal distribution
logData = log(sorted_Data);
[W, pvalLogNormal] = NormalDistributionTest(logData);
disp("p value for log normal distribution test :");
disp(pvalLogNormal);

%% Testing the Data with Exponential Distribution
[B] = ExponentialTest(sorted_Data, length(sorted_Data));
disp("B test value for exponential test :");
disp(B);

%% Testing the Data with Weibull distribution
[M, dof1, dof2] = WeibullTest(sorted_Data, length(sorted_Data));
disp("M value for Weibull distribution test :");
disp(M);

%% Selecting the distribution as Engineer request
disp("Input the appropriate distribution!");
distribution = input("Normal, Log-Normal, Exponential, Weibull? : ", "s");

t = 0:10000;
[hr, R, PDF, MTTF, RMTTF] = Cases(Data, distribution, t);

disp("Predicted failure in operation hour :");
disp(MTTF);

figure(1,"position",[100 100 1600 900]);...get(0,"screensize"));
subplot(2,2, 1:2);
plot(t, PDF);
grid on;
xlabel("Time (hours)");
ylabel("PDF (-)");
title(sprintf("Probability Density Function"));

subplot(2,2,3);
plot(t, R); 
hold on;
plot(MTTF, RMTTF, "ok");
xlabel("Time (hours)");
ylabel("Reliability (-)");
title("Reliability plot");
grid on;
ylim([0 1.01]);
xticks([0 2500 MTTF 5000 7500 10000]);
xticklabels({"0", "2500", MTTF, "5000", "7500", "10000"});
legend("Reliability","Predicted Failure");

subplot(2,2,4);
plot(t, hr);
xlabel("Time (hours)");
ylabel("Hazard rate (-)");
title("Hazard rate plot");
grid on;
ylim([0 1.1*max(hr)]);


%% Everything from line 56 untill 111 is taken from
%% RAMS and LCC Engineering for Railway Industry: Analysis, Modelling, and Optimization
%% Eduardo Calixto
%% 2019
%% The MTTF value is taken from derivation through Gamma Function
%% For More information about MTTF of Weibull Distribution:
%% http://reliawiki.org/index.php/The_Weibull_Distribution