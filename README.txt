Prerequisition package to run Main script:
1. IO ver 2.4.13
2. statistics 1.4.1
3. symbolic 2.8.0

Place the excel work in the same folder as Main.m to avoid any further error. Change the file name as your file name, the sheet name too. 
You can skip the load xls part if your txt data is available, please change the Main.m file appropriately. 
Don't forget to change the time if needed. The Data is assumed to be right censored.

Please refer to:
https://github.com/cbm755/octsympy/wiki/Notes-on-Windows-installation
for symbolic package download

For the xlsx data:
1. The rightmost data should be time to failure (TTF)
2. The second rightmost data should be the number of failed components with same serial number