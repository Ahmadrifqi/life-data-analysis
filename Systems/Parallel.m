function Parallel_Rel = Parallel(Reliability)
%% Reliability of a parallel system
%% Please read NoticeMeTT.txt
%% Source:
%% https://www.intechopen.com/books/concise-reliability-for-engineers/reliability-of-systems
%% This Script was created by
%% Ahmad Rifqi

%% [n l] = size(Reliability);
%% Parallel_Rel= ones(n,1);
%% for i =1:n
%%	for j =1:l
%%		Parallel_Rel(i) = Parallel_Rel(i,1) * (1-Reliability(i,j));
%%	end
%%	Parallel_Rel(i) = 1 - Parallel_Rel(i);
%% end

[n l] = size(Reliability);
Parallel_Rel= ones(n,1);

for i =1:n
	for j =1:l
		Parallel_Rel(i,1) = Parallel_Rel(i,1) * (1-Reliability(i,j));
	end
	Parallel_Rel(i,1) = 1 - Parallel_Rel(i,1);
end