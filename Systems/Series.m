function Series_Rel = Series(Reliability)
%% Reliability of a series system
%% Please read NoticeMeTT.txt
%% Source:
%% https://www.intechopen.com/books/concise-reliability-for-engineers/reliability-of-systems
%% This Script was created by
%% Ahmad Rifqi

%% [n l] = size(Reliability);
%% Series_Rel= ones(n,1);
%% for i =1:n
%%	for j =1:l
%%		Series_Rel(i) = Series_Rel(i,1) * Reliability(i,j);
%%	end
%% end

[n l] = size(Reliability);
Series_Rel= ones(n,1);

for i =1:n
	for j =1:l
		Series_Rel(i) = Series_Rel(i,1) * Reliability(i,j);
	end
end