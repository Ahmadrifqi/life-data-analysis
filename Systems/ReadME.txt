An example to utilize these functions properly:
Consider a system with:

sub-system 1 with reliability R1
sub-system 2 with reliability R2
sub-system 3 with reliability R3
sub-system 4 with reliability R4
In series configuration 

The sub-system 3 consists of:
sub-system 3.1 with reliability R3.1
sub-system 3.2 with reliability R3.2
sub-system 3.3 with reliability R3.3
In Parallel Configuration
then,
R3 = Parallel([R3.1 R3.2 R3.3]);

The sub-system 4 consists of:
sub-system 4.1 with reliability R4.1
sub-system 4.2 with reliability R4.2
In Parallel Configuration
then,
R4 = Parallel([R4.1 R4.2]);

The system reliability can be obtained by:
R_System = Series([R1 R2 Parallel([R3.1 R3.2 R3.3]) Parallel([R4.1 R4.2]));


The System Block Diagram:
			 |---R3.1---|
			 |			|	|---R.41---|
---R1---R2---|---R3.2---|---|		   |---
			 |			|	|---R.42---|
			 |---R3.3---|