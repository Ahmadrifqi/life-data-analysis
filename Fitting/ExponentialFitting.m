function lambda = ExponentialFitting(Data)
%% This Script was created by:
%% Ahmad Rifqi

%% Data = sort(Data);
%% r = length(Data);
%% biased_lambda = 1/mean(Data);
%% lambda = biased_lambda;

Data = sort(Data);
r = length(Data);

biased_lambda = 1/mean(Data);
lambda = (r-1)/r*biased_lambda;