function [shape, scale] = WeibullFittingLSE(Data)
%% Weibull Distribution Parameter approximation
%% Using Weighted Least Squares Estimation
%% Source:
%% Comparison of Four Methods for Estimating the Weibull Distribution Parameters
%% Ivana Pobočíková and Zuzana Sedliačková
%% 2014

%% This Script was created by:
%% Ahmad Rifqi

%% Data = sort(Data);
%% r = length(Data);
%% for i =1:r
%%	Fx = i/(r+1);
%%	w(i) = ((1-Fx)*(log(1-Fx)))^2;
%%	y(i) = log(-log(1-Fx));
%% end
%% for i = 1:r
%%	wlog(i) = w(i) * log(Data(i));
%%	wlogy(i) = wlog(i) * y(i);
%%	wy(i) = w(i)*y(i);
%%	wlog2(i) = wlog(i) * log(Data(i));
%% end
%% Num = sum(w)*sum(wlogy) - sum(wlog)*sum(wy);
%% Den = sum(w)*sum(wlog2) - sum(wlog)^2;
%% shape = Num/Den;
%% Numerator = sum(wy) - shape * sum(wlog);
%% Denumerator = shape * sum(w);
%% scale = exp(-Numerator/Denumerator);

Data = sort(Data);
r = length(Data);

for i = 1:r
	Fx = (i - 0)/(r + 1);
	w(i) = ((1-Fx)*(log(1-Fx)))^2;
	y(i) = log(-log(1-Fx));
end

% Defining Numerator and Denumerator for Shape estimation
for i = 1:r
	wlog(i) = w(i) * log(Data(i));
	wlogy(i) = wlog(i) * y(i);
	wy(i) = w(i)*y(i);
	wlog2(i) = wlog(i) * log(Data(i));
end
Num = sum(w)*sum(wlogy) - sum(wlog)*sum(wy);
Den = sum(w)*sum(wlog2) - sum(wlog)^2;
shape = Num/Den;

% Defining Numerator and Denumerator for Scale estimation
Numerator = sum(wy) - shape * sum(wlog);
Denumerator = shape * sum(w);
scale = exp(-Numerator/Denumerator);