function [mu, sigma] = NormalDistributionFitting(Data)
%% This script was created by :
%% Ahmad Rifqi

%% mu = mean(Data);
%% sigma = std(Data);

mu = mean(Data);
sigma = std(Data);