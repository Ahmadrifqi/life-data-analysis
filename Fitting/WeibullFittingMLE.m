function [shape, scale] = WeibullFittingMLE(Data)
%% Weibull Distribution Parameter approximation
%% Using Maximum Likelihood Estimation
%% Source:
%% Comparison of Four Methods for Estimating the Weibull Distribution Parameters
%% Ivana Pobočíková and Zuzana Sedliačková
%% 2014

%% This Script was created by:
%% Ahmad Rifqi

%% Data = sort(Data);
%% r = length(Data);
%% third = sum(log(Data))/r;
%% for i = 0.01:0.001:10
%%	first = 1/i;
%%	second = sum((Data.^i) .* log (Data)) / sum(Data.^i);
%%	if first - second + third < 0.0001
%%		shape = i;
%%		break;
%%	end
%% end
%% scale = (sum(Data.^shape)/r)^(1/shape);

Data = sort(Data);
r = length(Data);
third = sum(log(Data))/r;

for i = 0.01:0.001:10
	first = 1/i;
	second = sum((Data.^i) .* log (Data)) / sum(Data.^i);
	if first - second + third < 0.0001
		shape = i;
		break;
	end
end

scale = (sum(Data.^shape)/r)^(1/shape);