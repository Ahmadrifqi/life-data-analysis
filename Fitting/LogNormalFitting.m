function [shape, scale] = LogNormalFitting(Data)
%% Script Created by:
%% Ahmad Rifqi

%% Data = log(Data);
%% mu = mean(Data);
%% stdev = std(Data);
%% location = mu;
%% scale = stdev;

Data = log(Data);
mu = mean(Data);
stdev = std(Data);

shape = mu;
scale = stdev;