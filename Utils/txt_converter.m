function[name_file] = txt_converter(filename, sheet);
%% This script is created by:
%% Ahmad Rifqi

%% name_file = [sheet '.txt'];
%% fid = fopen(name_file, 'w+');
%% Raw = xlsread(filename, sheet);
%% row = length(Raw);
%% column = length(Raw(1,:));
%% Data = Raw(:,column-1:column);
%% final = sum(Data(:,1));
%% output = zeros(final, 1);
%% num = 1;
%% for i=1:length(Data)
%%   output(num,1) = Data(i,2);
%%   fprintf(fid, '%d', output(num,1));
%%   fprintf(fid, '\n');
%%   num = num+1;
%%   if Data(i,1) ~= 0
%%     param = 1;
%%     while param < Data(i,1)
%%       output(num,1) = Data(i,2);
%%       fprintf(fid, '%d', output(num,1));
%%       fprintf(fid, '\n');
%%       num = num+1;
%%       param = param+1;
%%     end
%%   end
%% end
%% fclose(fid);

%The number of failures and time to fail should be on the right consecutively
name_file = [sheet '.txt'];
fid = fopen(name_file, 'w+');
Raw = xlsread(filename, sheet);
row = length(Raw);
column = length(Raw(1,:));
Data = Raw(:,column-1:column);
final = sum(Data(:,1));
output = zeros(final, 1);
num = 1;
for i=1:length(Data)
  output(num,1) = Data(i,2);
  fprintf(fid, '%d', output(num,1));
  fprintf(fid, '\n');
  num = num+1;
  if Data(i,1) ~= 0
    param = 1;
    while param < Data(i,1)
      output(num,1) = Data(i,2);
      fprintf(fid, '%d', output(num,1));
      fprintf(fid, '\n');
      num = num+1;
      param = param+1;
    end
  end
end
fclose(fid);