function [hr, R, PDF, MTTF, RMTTF] = Cases(Data, distribution, x)

switch (distribution)
	case "Normal"
		[mu, sigma] = NormalDistributionFitting(Data);
		MU = (mu); SIG = (sigma);
		f = @(t) 1/(SIG*sqrt(2*pi())).*exp(-(1/2)*((t-MU)/SIG).^2);
		%mttf = @(t) t/(SIG*sqrt(2*pi())).*exp(-(1/2)*((t-MU)/SIG).^2);
		MTTF = mu; %quadgk(mttf, 0, Inf);
		for i=1:length(x)
			R(i) = quadgk(f, x(i), Inf);
			hr(i) = f(x(i))/R(i);
			PDF(i) = f(x(i));
			if abs(MTTF - x(i)) <= 0.99
				RMTTF = R(i);
			end
		end
		
	case "Log-Normal"
		x(1) = 1;
		[location, scale] = LogNormalFitting(Data);
		LOC = round(location); SCA = round(scale);
		f = @(t) (1/(t.*SCA*sqrt(2*pi))).*exp(-(1/2).*((log(t)-LOC)/(SCA)).^2);
		mttf = @(t) (t/(t.*LOC*sqrt(2*pi))).*exp(-(1/2).*((log(t)-LOC)/(SCA)).^2);
		MTTF = quad(mttf, 0, Inf);
		for i=1:length(x)
			R(i) = quad(f, x(i), Inf);
			hr(i) = f(x(i))/R(i);
			PDF(i) = f(x(i));
			if abs(MTTF - x(i)) <= 0.99
				RMTTF = R(i);
			end
		end
		
	case "Exponential"
		[lambda] = ExponentialFitting(Data);
		f = @(t) lambda.*exp(-lambda.*t);
		mttf = @(t) t.*lambda.*exp(-lambda.*t);
		MTTF = quadgk(mttf, 0.01, Inf);
		for i=1:length(x)
			R(i) = quadgk(f, x(i), Inf);
			hr(i) = f(x(i))/R(i);
			PDF(i) = f(x(i));
			if abs(MTTF - x(i)) <= 0.99
				RMTTF = R(i);
			end
		end
		RMTTF = exp(-(lambda*MTTF));
		
	case "Weibull"
		[shape, scale] = WeibullFittingMLE(Data);
		f = @(t) (shape/scale)*((t/scale).^(shape-1)).*exp(-(t/scale).^(shape-1));
%		mttf = @(t) t.*(shape/scale).*((t/scale).^(shape-1)).*exp(-(t/scale).^(shape-1));
		Gamm = @(t) t.^((1/shape)+1).*exp(-t);
		MTTF = scale*quadgk(Gamm, 0, Inf);
		for i=1:length(x)
			R(i) = exp(-(x(i)/scale)^shape);
			hr(i) = f(x(i))/R(i);
			PDF(i) = f(x(i)); 
			if abs(MTTF - x(i)) <= 0.99
				RMTTF = R(i);
			end
		end
    disp(["Shape : " num2str(shape)]);
    disp(["Scale : " num2str(scale)]); 
		
	otherwise
		disp("Error inputing the appropriate distribution");
end